Path Planner
============

Generates a controller input for the linear MPC controller to follow a path in GPS coordinates.
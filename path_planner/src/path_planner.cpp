/*
 * Copyright (C) 2015 Daniel Eckert, ASL, ETH Zurich, Switzerland
 * You can contact the author at <daeckert at student dot ethz dot ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "include/path_planner.h"

PathPlanner::PathPlanner()
{
	ros::NodeHandle nh_ = ros::NodeHandle("path_planner");

	// Read settings
	if (!nh_.getParam("base_longitude", base_pos_[0]))
		ROS_WARN_STREAM("Base Position longitude not specified. Using 0.");
	if (!nh_.getParam("base_latitude", base_pos_[1]))
		ROS_WARN_STREAM("Base Position latitude not specified. Using 0.");
	if (!nh_.getParam("base_altitude", base_pos_[2]))
		ROS_WARN_STREAM("Base Position altitude not specified. Using 0.");
	if (!nh_.getParam("waypoint_file", waypoint_file_))
		ROS_WARN_STREAM("path_file parameter not set. Using '/home/user/mav_paths/waypoints.txt'.");
	if (!nh_.getParam("msf_pose", msf_pose_))
		ROS_WARN_STREAM("msf_pose parameter not set. Using 'msf_core/pose'.");
	if (!nh_.getParam("velocity_factor", velocity_factor_))
		ROS_WARN_STREAM("velocity_factor parameter not set. Using 1.");
	if (!nh_.getParam("use_baseline_reference", use_baseline_reference_))
		ROS_WARN_STREAM("use_baseline_reference parameter not set. Using 'true'.");
	if (!nh_.getParam("absolute_yaw", absolute_yaw_))
		ROS_WARN_STREAM("adjust_pose parameter not set. Using 'true'.");

	// Initialize publihsers and subscribers
	if(use_baseline_reference_)
		sub_ref_ = ros::NodeHandle("navsatfix_conversion").subscribe("baseline_reference", 1000,
											&PathPlanner::BaselineRefCallback, this);
	else
		sub_ref_ = ros::NodeHandle("navsatfix_conversion").subscribe("gps_reference", 1000,
											&PathPlanner::GpsRefCallback, this);

	ROS_WARN_STREAM("sub_ref_ topic: " << sub_ref_.getTopic());

	//pub_ = nh.advertise<mav_msgs::EigenTrajectoryPoint>("orientation", 10);
	pub_ = nh_.advertise<geometry_msgs::PoseStamped>("target_pose", 10);

	// Calculate position scaling factors (m to lat/lon)
	lon_to_m_ = 111412.84 * cos(base_pos_[0] * M_PI/180)
				- 93.5 * cos(3 * base_pos_[0] * M_PI/180)
				+ 0.118 * cos(5 * base_pos_[0] * M_PI/180);
	lat_to_m_ = 111132.954
				- 559.822 * cos(2 * base_pos_[0] * M_PI/180)
				+ 1.175 * cos(4 * base_pos_[0] * M_PI/180)
				- 0.0023 * cos(6 * base_pos_[0] * M_PI/180);
	m_to_lat_ = 1/lat_to_m_;
	m_to_lon_ = 1/lon_to_m_;

	// Read waypoint file
	ROS_INFO_STREAM("Reading waypoint file...");
	std::ifstream inputFile(waypoint_file_.c_str());

	if (inputFile==NULL) {
		ROS_ERROR_STREAM("Couldn't read waypoint file!");
		perror ("The following error occurred");
		return;
	}

	std::string line;
	while(std::getline(inputFile, line)) {
		if(line.substr(0,1) != "%") {
			waypoint p;
			std::vector<std::string> elems;
			boost::split(elems, line, boost::is_any_of(","));

			p.enu[0] = std::atof(elems[0].c_str());
			p.enu[1] = std::atof(elems[1].c_str());
			p.enu[2] = std::atof(elems[2].c_str());
			p.yaw = std::atof(elems[3].c_str());
			p.vel = std::atof(elems[4].c_str());
			p.time = std::atof(elems[5].c_str());

			path_.push_back(p);
		}
	}

	ROS_INFO_STREAM("Waypoint file successfully imported. Waiting for GPS/Baseline reference.");
	inputFile.close();

	// Initialize default orientation
	orientation_ = tf::createQuaternionFromYaw(0);
	carrot_.pose.orientation.w = orientation_.w();
	carrot_.pose.orientation.x = orientation_.x();
	carrot_.pose.orientation.y = orientation_.y();
	carrot_.pose.orientation.z = orientation_.z();
}

// Wait for Baseline reference message coming from Position Sensor
void PathPlanner::BaselineRefCallback(const geometry_msgs::Vector3ConstPtr &msg)
{
	if(!initialized_) {
		gps_reference_ = *msg;
		if(use_baseline_reference_) {
			ROS_INFO_STREAM("PathPlanner received Baseline reference.");

			// Converting waypoints to local ENU coordinate system
			std::list<waypoint>::iterator it;
			for (it = path_.begin(); it != path_.end(); ++it) {
				(*it).enu[0] = ((*it).enu[0] - base_pos_[0] - gps_reference_.x * m_to_lon_)*lon_to_m_;
				(*it).enu[1] = ((*it).enu[1] - base_pos_[1] - gps_reference_.y * m_to_lon_)*lat_to_m_;
				(*it).enu[2] =  (*it).enu[2] - base_pos_[2] - gps_reference_.z;
			}
		}

		ROS_INFO_STREAM("Waypoints successfully transformed to ENU.");

		target_ = path_.front();
		path_.pop_front();

		// Activate Callback for Pose messages
		sub_ = nh_.subscribe(msf_pose_, 1000, &PathPlanner::messageCallback, this);

		initialized_ = true;
	}
}

// Wait for GPS reference message coming from Position Sensor
void PathPlanner::GpsRefCallback(const geometry_msgs::Vector3ConstPtr &msg)
{
	if(!initialized_) {
		gps_reference_ = *msg;
		if(!use_baseline_reference_) {
			ROS_INFO_STREAM("PathPlanner received GPS reference.");

			// Converting waypoints to local ENU coordinate system
			std::list<waypoint>::iterator it;
			for (it = path_.begin(); it != path_.end(); ++it) {
				(*it).enu[0] = ((*it).enu[0] - gps_reference_.y)*lon_to_m_;
				(*it).enu[1] = ((*it).enu[1] - gps_reference_.x)*lat_to_m_;
				(*it).enu[2] = (*it).enu[2] - gps_reference_.z;
			}
		}

		ROS_INFO_STREAM("Waypoints successfully transformed to ENU.");

		target_ = path_.front();
		path_.pop_front();

		// Activate Callback for Pose messages
		sub_ = nh_.subscribe(msf_pose_, 1000, &PathPlanner::messageCallback, this);

		initialized_ = true;
	}
}

void PathPlanner::messageCallback(const geometry_msgs::PoseWithCovarianceStampedConstPtr &msg)
{
	// Limit update rate to 100Hz
	double time_diff = msg->header.stamp.sec - time_old_;
	if(msg->header.stamp.nsec - time_old_ < 0.01)
		return;

	time_old_ = msg->header.stamp.sec;

	// Calculate vector from current position to target
	Eigen::Vector3d target_offset;
	target_offset << target_.enu[0] - msg->pose.pose.position.x,
					 target_.enu[1] - msg->pose.pose.position.y,
					 target_.enu[2] - msg->pose.pose.position.z;

	double dist = target_offset.norm();

	// If we are close to target, wait or take next waypoint
	if(dist < 0.5) {
		// If we arrived at the last waypoint, treat it specially
		if(path_.size() == 0)
			goto LastWaypoint;

		// If a time is specified, wait
		if(target_.time != 0) {
			// In first iteration, set the target as carrot
			if(wait_time_ == -1) {
				wait_time_ = target_.time;
				ROS_INFO_STREAM("Reached Waypoint: " << target_.enu.transpose());
				ROS_INFO_STREAM("Waiting at waypoint for " << target_.time << " seconds.");

				double time = ros::Time::now().toSec();

				carrot_.header.stamp = ros::Time::now();
				carrot_.pose.position.x = target_.enu[0];
				carrot_.pose.position.y = target_.enu[1];
				carrot_.pose.position.z = target_.enu[2];
				pub_.publish(carrot_);
			}
			else
				wait_time_ -= time_diff;

			// If wait time is over, goto next waypoint
			if(wait_time_ < 0) {
				wait_time_ = -1;
				goto NextWaypoint;
			}
			else
				return;
		}
		// If no time is specified, take next waypoint
		else {
			ROS_INFO_STREAM("Reached Waypoint: " << target_.enu.transpose());

			// Take next waypoint
			NextWaypoint:
			target_ = path_.front();
			path_.pop_front();
			ROS_INFO_STREAM("Next target: " << target_.enu.transpose());
		}
	}

	// Generate and publish carrot
	dir_ = target_offset;
	dir_.normalize();

	carrot_.header.stamp = ros::Time::now();
	carrot_.pose.position.x = msg->pose.pose.position.x + velocity_factor_*target_.vel*dir_[0];
	carrot_.pose.position.y = msg->pose.pose.position.y + velocity_factor_*target_.vel*dir_[1];
	carrot_.pose.position.z = msg->pose.pose.position.z + velocity_factor_*target_.vel*dir_[2];

	// Calculate yaw
	double yaw;
	if(absolute_yaw_) {
		yaw = target_.yaw;
	}
	else {
		Eigen::Vector2d ne(target_offset[0], target_offset[1]);

		if(ne.norm() < 0.5)		// If the target is closer than 0.5m, we will use the last  orientation
			goto PublishCarrot;

		yaw = atan2(ne[1], ne[0])*180/M_PI + target_.yaw;
	}

	orientation_ = tf::createQuaternionFromYaw(yaw*M_PI/180);
	carrot_.pose.orientation.w = orientation_.w();
	carrot_.pose.orientation.x = orientation_.x();
	carrot_.pose.orientation.y = orientation_.y();
	carrot_.pose.orientation.z = orientation_.z();

	PublishCarrot:
	pub_.publish(carrot_);
	return;


	// If we reached the last waypoint, use it directly as controller input
	LastWaypoint:
	ROS_INFO_STREAM_ONCE("Reached last waypoint!");

	carrot_.header.stamp = ros::Time::now();
	carrot_.pose.position.x = target_.enu[0];
	carrot_.pose.position.y = target_.enu[1];
	carrot_.pose.position.z = target_.enu[2];

	pub_.publish(carrot_);
}

PathPlanner::~PathPlanner()
{
}

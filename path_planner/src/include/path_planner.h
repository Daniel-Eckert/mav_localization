/*
 * Copyright (C) 2015 Daniel Eckert, ASL, ETH Zurich, Switzerland
 * You can contact the author at <daeckert at student dot ethz dot ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PATH_PLANNER_H
#define PATH_PLANNER_H

#include <fstream>
#include "ros/ros.h"
#include "ros/time.h"

#include <eigen3/Eigen/Eigen>
#include <mav_msgs/eigen_mav_msgs.h>
#include <boost/algorithm/string.hpp>

#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/Vector3.h"
#include "sensor_msgs/NavSatFix.h"
#include "sensor_msgs/MagneticField.h"
#include "geometry_msgs/PoseStamped.h"
#include "tf/tf.h"

// Custom message includes. Auto-generated from msg/ directory.
// #include "path_planner/NodeExampleData.h"

class PathPlanner
{
private:
  ros::Subscriber sub_;
  ros::Subscriber sub_ref_;
  ros::Publisher pub_;
  ros::NodeHandle nh_;
  geometry_msgs::Vector3 gps_reference_;
  geometry_msgs::Vector3 baseline_reference_;

  bool initialized_;
  double lon_to_m_, lat_to_m_, m_to_lat_, m_to_lon_;

  // Helper Variables
  int time_old_ = 0;
  double wait_time_;
  Eigen::Vector3d dir_;  		// Direction of carrot
  tf::Quaternion orientation_;	// Orientation of carrot

  struct waypoint {
	  Eigen::Vector3d enu;
	  double vel;
	  double time;
	  double yaw;
  };

  //mav_msgs::EigenTrajectoryPoint target_;
  geometry_msgs::PoseStamped carrot_;
  std::list<waypoint> path_;
  waypoint target_;

  // YAML file settings
  Eigen::Vector3d base_pos_;
  std::string waypoint_file_ = "/home/user/mav_paths/waypoints.txt";
  std::string msf_pose_ = "/msf_core/pose";
  double velocity_factor_ = 1;
  bool use_baseline_reference_ = true;
  bool absolute_yaw_ = true;

public:
  PathPlanner();
  ~PathPlanner();

  void publishMessage(ros::Publisher *pub_message);

  void readPath(std::string filename);

  //! Callback functions
  void messageCallback(const geometry_msgs::PoseWithCovarianceStampedConstPtr &msg);
  void GpsRefCallback(const geometry_msgs::Vector3ConstPtr &msg);
  void BaselineRefCallback(const geometry_msgs::Vector3ConstPtr &msg);
};

#endif // PATH_PLANNER_H

# Piksi-Pose-GPS Sensor Fusion #

This section provides a starting point for all those who want to run the UAV state estimator, developed in this  this [master thesis](https://bitbucket.org/Daniel-Eckert/mav_localization/raw/a7206b8e3ddefee5bedce81bd66059e978ec0058/Outdoor_UAV_Localization.pdf). The system fuses RTK GPS, visual-inertial pose estimation and SPP GPS, and achieves accuracies of less than 10cm standard deviation. For a detailed description of the systems functioning, features, and performance please refer to the [thesis](https://bitbucket.org/Daniel-Eckert/mav_localization/raw/a7206b8e3ddefee5bedce81bd66059e978ec0058/Outdoor_UAV_Localization.pdf).

The MAV localization repository contains functions for path planning and visualization as part of the system. The specific documentation is found one section below.

## Getting Started ##

The state estimator requires several repositories which all contain their individual documentations. The following are the minimum required packages to run the state estimator:

* [Multi Sensor Fusion framework (MSF), Piksi-Pose-GPS implementation](https://github.com/ethz-asl/ethzasl_msf_devel/tree/dev_feature/covariance_estimation_piksi): Contains the Extended Kalman Filter
* [Piksi Driver](https://bitbucket.org/Daniel-Eckert/piksi_node): RTK GPS ROS driver
* [VI-sensor node](https://github.com/ethz-asl/visensor_node): ROS driver for the VI-sensor
* [ROVIO](https://github.com/Daniel-Eckert/rovio): Visual-Inertial odometry framework, used for pose estimation (this repository is a fork, which adds an additional output message to ROVIO: the pose estimate in the IMU frame. Furthermore parameters are slightly adjusted for performance on slower machines.)

Additional repositories, which are recommended to use:

* [MAV Localization](https://bitbucket.org/Daniel-Eckert/mav_localization): Contains tools for path planning and trajectory visualization with Google Earth/Google Maps, which are specifically developed for this state estimator.
* [RotorS Simulator (Piksi Branch)](https://github.com/ethz-asl/rotors_simulator/tree/feature/piksi_simulation/): Contains the RotorS simulator,with an additional plugin to simulate the Piksi.
* [Geodetic Utils](https://github.com/ethz-asl/geodetic_utils) (for use on a UAV): On the Asctec Colibri drone, magnetic interferences distorted the magnetometer measurements. This repository provides functions for magnetometer calibration in the IMU compass node.

Before running the system it is critical to have a basic understanding of all the individual repositories. 

A good way to get familiar with the system is the simulation. The MAV Localization repository contains a launch file called *simulation.launch*, which will run the state estimator as well as the path planner and real-time path visualization tools in a Gazebo environment. If everything works, the UAV will fly a square pattern,and hover at each waypoint for 3 seconds.

Before running the system with real hardware, it is important to get familiar with the RTK GPS setup. Chapter 2 of the [thesis](https://bitbucket.org/Daniel-Eckert/mav_localization/raw/a7206b8e3ddefee5bedce81bd66059e978ec0058/Outdoor_UAV_Localization.pdf) gives an introduction on how RTK GPS works. To get some experience with the Piksi module, a good starting point is the [official documentation](http://docs.swiftnav.com/wiki/Main_Page). The [Piksi tools](https://github.com/swift-nav/piksi_tools) repository provides all drivers and software for operating the Piksi (independent of ROS).

A helpful launch file to get familiar with the state estimator setup on a UAV can be found in the MAV Localization repository at */launch/colibri_piksi.launch*. This launch file is written for the Asctec Colibri drone.

People who are interested mainly in the covariance estimation techniques used in the state estimator, but don't actually want to run the whole system can find a tutorial on how to use the covariance estimation with arbitrary sensor types in the [MSF repository](https://github.com/ethz-asl/ethzasl_msf_devel/tree/dev_feature/covariance_estimation_piksi).

# MAV Localization #

Contains tools for UAV path planning in GPS coordinates and trajectory visualization with Goolge Maps/Google Earth. Requires the Piksi-Pose-GPS sensor fusion setup, described in the section above.

## Dependencies ##

* ROS
* Multi Sensor Fusion framework, Piksi-Pose-GPS implementation (https://github.com/ethz-asl/ethzasl_msf_devel/tree/dev_feature/covariance_estimation_piksi)

## NavSatFix Conversion ##

This node uses the pose output of the state estimation, and converts it to global GPS coordinates. The path can be exported as a kml file. For real time visualization, the output is compatible with the path visualization GUI Rendezvous. The functioning is explained in section 4.2.1 of the [thesis](https://bitbucket.org/Daniel-Eckert/mav_localization/raw/a7206b8e3ddefee5bedce81bd66059e978ec0058/Outdoor_UAV_Localization.pdf).

### Setup ###

Before using the system, the following parameters need to be adjusted in the settings file */cfg/navsatfix_conversion.yaml*:

* 'base_longitude/latitude/altitude': Position of the base station in GPS coordinates.
* 'msf_pose': ROS topic of the pose estimate in ENU coordinates (output of the state estimator).
* 'use_baseline_reference': Specifies, whether the Piksi is used with PiksiBaseline or PiksiRtkPos messages. (By default the system uses PiksiBaseline messages, and the parameter is set to true)

### Use with Rendezvous ###

The output of the Navsatfix conversion node is defined for compatibility with [Rendezvous](https://github.com/fly4smartcity/rendezvous), which is an open source tool for visualization of the UAV path through the Google Maps Javascript API. The software can be setup as follows:

1. Follow the [tutorial](https://github.com/fly4smartcity/rendezvous) to install Rendezvous and Rosbridge.
2. Open *rendezvous/js/subscriber_ros_gui.js*, and adjust the topics that Redezvous subscribes to as follows:
```
#!command
	
var navSatFixListener = new ROSLIB.Topic({
    ros: ros,
    name: '/navsatfix_conversion/navsatfix', //topic name
    messageType: 'sensor_msgs/NavSatFix' //message Type
});

var magneticFieldListener = new ROSLIB.Topic({
    ros: ros,
    name: '/navsatfix_conversion/orientation', //topic name
    messageType: 'sensor_msgs/MagneticField' //message Type
});
```

The state estimator and Rosbridge need to be run simultaneously. The UAV path will be visualized in the file *rendezvous/index.html* (requires an internet connection).

## Path Planner ##

This node takes a waypoint file (in GPS coordinates) as input, and generates a target for the [linear MPC controller](https://github.com/ethz-asl/mav_control)(in ENU coordinates), such that the MAV follows the specified path. Yaw, speed, and the hovering time for each waypoint can be specified. Underlying principles and equations are explained in the [thesis](https://bitbucket.org/Daniel-Eckert/mav_localization/raw/a7206b8e3ddefee5bedce81bd66059e978ec0058/Outdoor_UAV_Localization.pdf), section 4.2.2.

### Settings ###

Before using the path planner, the following parameters need to be adjusted in the settings file */cfg/path_planner.yaml*:

* 'base_longitude/latitude/altitude': Position of the base station in GPS coordinates.
* 'msf_pose': ROS topic of the UAV pose (output of the state estimator).
* 'use_baseline_reference': Specifies, whether the whether the Piksi is used with PiksiBaseline or PiksiRtkPos messages. (By default the system uses PiksiBaseline messages, and the parameter is set to true)
* 'waypoint_file': Path to the waypoint file.

### Generating a waypoint file ###

The path planner uses a text file with one waypoint per line as an input. Each waypoint has the following format:

*GPS-Longitude, GPS-Latitude, Relative-Altitude, Yaw, Velocity, Hovering-Time*

The GPS coordinates for the waypoint file can be obtained through Google-Earth. First a path is drawn with the 'Add Path' tool. Then, the path is exported as a kml file. The coordinates section in the kml file can directly be copied into the waypoint file.
The heights obtained from the kml file are all equal to zero, and therefore need to be adjusted manually. Heights are specified in the local ENU frame, which means that they are always relative to the point where the state estimator is initialized.

Besides coordinates, the yaw, velocity and hovering time for each waypoint need to be specified.

* *Yaw:* The 'absolute_yaw' parameter in the settings specifies, whether the yaw is applied as an absolute value (with respect to the east-axis), or as a relative value (with respect to the vector between the current position of the UAV and the next waypoint). In both cases, the yaw is specified in degrees.
* *Velocity:* Specifies the velocity with which the UAV flies towards this waypoint. This value should be chosen between 0 (standing still) and 1 (maximum velocity). In order to adjust the maximum velocity, the settings parameter 'velocity_factor' can be used. This parameter should be adjusted for each UAV, however 1 is usually a save value to start with.
* *Hovering Time:* Specifies the time that the UAV will hover at the respective waypoint.

# Scripts #

*estimate_imu_bias.py* can be used to estimate accelerometer, gyroscope and magnetometer biases of the Visual-Inertial (VI) sensor (http://wiki.ros.org/vi_sensor). Before running the script, place the VI-sensor on even ground (cameras facing down), with the front side oriented towards east. The script will take the "/cust_imu0" message as an input, and output the most recent estimate of the biases every two seconds.
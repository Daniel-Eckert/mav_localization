/*
 * Copyright (C) 2015 Daniel Eckert, ASL, ETH Zurich, Switzerland
 * You can contact the author at <daeckert at student dot ethz dot ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NAVSATFIX_CONVERSION_H
#define NAVSATFIX_CONVERSION_H

#include <fstream>
#include "ros/ros.h"
#include "ros/time.h"

#include <eigen3/Eigen/Eigen>

#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/Vector3.h"
#include "sensor_msgs/NavSatFix.h"
#include "sensor_msgs/MagneticField.h"

// Custom message includes. Auto-generated from msg/ directory.
// #include "navsatfix_conversion/NodeExampleData.h"

class NavsatfixConversion
{
private:
  ros::Subscriber sub_message_;
  ros::Subscriber sub_ref_gps_;
  ros::Subscriber sub_ref_baseline_;
  ros::Publisher pub_navsatfix_;
  ros::Publisher pub_mag_;
  geometry_msgs::Vector3 gps_reference_;
  std::ofstream outputFile_;

  bool file_ok_;
  double lon_to_m_, lat_to_m_, m_to_lat_, m_to_lon_;
  int time_old_ = 0;

  std::list<sensor_msgs::NavSatFix> coordinates_;

  // YAML file settings
  bool use_baseline_reference_ = true;
  bool generate_kml_ = true;
  bool publish_navsatfix_ = true;
  Eigen::Vector3d base_pos_;
  std::string kml_file_prefix_ = "msf_output_";
  std::string kml_folder_ = "/home/user/";
  std::string msf_pose_ = "msf_core/pose";

public:
  NavsatfixConversion();
  ~NavsatfixConversion();

  void publishMessage(ros::Publisher *pub_message);

  //! Callback functions
  void messageCallback(const geometry_msgs::PoseWithCovarianceStampedConstPtr &msg);
  void referenceCallbackGps(const geometry_msgs::Vector3ConstPtr &msg);
  void referenceCallbackBaseline(const geometry_msgs::Vector3ConstPtr &msg);
};

#endif // NAVSATFIX_CONVERSION_H

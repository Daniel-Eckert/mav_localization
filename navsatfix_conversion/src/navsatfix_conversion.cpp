/*
 * Copyright (C) 2015 Daniel Eckert, ASL, ETH Zurich, Switzerland
 * You can contact the author at <daeckert at student dot ethz dot ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "include/navsatfix_conversion.h"

NavsatfixConversion::NavsatfixConversion()
{
	ros::NodeHandle nh("navsatfix_conversion");

	// Read settings
	if (!nh.getParam("base_latitude", base_pos_[0]))
		ROS_WARN_STREAM("Base Position latitude not specified. Using 0.");
	if (!nh.getParam("base_longitude", base_pos_[1]))
		ROS_WARN_STREAM("Base Position longitude not specified. Using 0.");
	if (!nh.getParam("base_altitude", base_pos_[2]))
		ROS_WARN_STREAM("Base Position altitude not specified. Using 0.");
	if (!nh.getParam("publish_navsatfix", publish_navsatfix_))
		ROS_WARN_STREAM("publish_navsatfix parameter not set. Using 'true'.");
	if (!nh.getParam("generate_kml", generate_kml_))
		ROS_WARN_STREAM("generate_kml parameter not set. Using 'true'.");
	if (!nh.getParam("kml_folder", kml_folder_))
		ROS_WARN_STREAM("kml_folder parameter not set. Using '/home/user/'.");
	if (!nh.getParam("kml_file_prefix", kml_file_prefix_))
		ROS_WARN_STREAM("kml_file_prefix parameter not set. Using 'msf_output_'.");
	if (!nh.getParam("use_baseline_reference", use_baseline_reference_))
		ROS_WARN_STREAM("use_baseline_reference parameter not set. Using 'true'.");
	if (!nh.getParam("msf_pose", msf_pose_))
		ROS_WARN_STREAM("msf_pose parameter not set. Using 'msf_core/pose'.");

	// Initialize publihsers and subscribers
	sub_ref_gps_ = nh.subscribe("gps_reference", 1000,
								&NavsatfixConversion::referenceCallbackGps, this);
	sub_ref_baseline_ = nh.subscribe("baseline_reference", 1000,
									&NavsatfixConversion::referenceCallbackBaseline, this);

	sub_message_ = nh.subscribe(msf_pose_, 1000, &NavsatfixConversion::messageCallback, this);
	pub_navsatfix_ = nh.advertise<sensor_msgs::NavSatFix>("navsatfix", 10);
	pub_mag_ = nh.advertise<sensor_msgs::MagneticField>("orientation", 10);

	// Calculate position scaling factors (between m and lat/lon)
	lon_to_m_ = 111412.84 * cos(base_pos_[0]*M_PI/180)
				- 93.5 * cos(3 * base_pos_[0] * M_PI/180)
				+ 0.118 * cos(5 * base_pos_[0] * M_PI/180);
	lat_to_m_ = 111132.954
				- 559.822 * cos(2 * base_pos_[0] * M_PI/180)
				+ 1.175 * cos(4 * base_pos_[0] * M_PI/180)
				- 0.0023 * cos(6 * base_pos_[0] * M_PI/180);
	m_to_lat_ = 1/lat_to_m_;
	m_to_lon_ = 1/lon_to_m_;

	// Prepare for writing kml file
	if(generate_kml_) {
		// generate file name
		std::time_t t = time(0);   				// get time now
		struct tm * now = localtime( & t );
		std::stringstream file;
		file << kml_folder_ << kml_file_prefix_ << now->tm_mday << "-" << now->tm_mon + 1
			 << "-" << now->tm_year + 1900 << "_" << now->tm_hour << ":" << now->tm_min << ".kml";

		// Open kml file
		outputFile_.open(file.str().c_str());

		bool file_ok_ = false;
		if (outputFile_==NULL) {
			ROS_ERROR_STREAM("Couldn't create kml file!");
			perror ("The following error occurred");
		}
		else {
			ROS_INFO_STREAM("Generated file " << file.str().c_str() << ".");
			file_ok_ = true;
		}
	}
}

// Wait for GPS reference message
void NavsatfixConversion::referenceCallbackGps(const geometry_msgs::Vector3ConstPtr &msg)
{
	if(!use_baseline_reference_ && (msg->x !=  0 || msg->y !=  0 || msg->z !=  0)) {
		gps_reference_ = *msg;
		ROS_INFO_STREAM("NavsatfixConversion received GPS reference.");
	}
}

// Wait for baseline reference message
void NavsatfixConversion::referenceCallbackBaseline(const geometry_msgs::Vector3ConstPtr &msg)
{
	if(use_baseline_reference_) {
		gps_reference_ = *msg;
		ROS_INFO_STREAM("NavsatfixConversion received Piksi baseline reference.");
	}
}

void NavsatfixConversion::messageCallback(const geometry_msgs::PoseWithCovarianceStampedConstPtr &msg)
{
	// Limit update rate to 5Hz
	if(msg->header.stamp.sec - time_old_ < 0.2)
		return;

	time_old_ = msg->header.stamp.nsec;

	// Generate NavSatFix message
	sensor_msgs::NavSatFix navsatfix;
	navsatfix.header = msg->header;

	if(use_baseline_reference_) {
		navsatfix.latitude = base_pos_[0] + m_to_lat_ * (gps_reference_.y + msg->pose.pose.position.y);
		navsatfix.longitude = base_pos_[1] + m_to_lon_ * (gps_reference_.x + msg->pose.pose.position.x);
		navsatfix.altitude = base_pos_[2] + gps_reference_.z + msg->pose.pose.position.z;
	}
	else {
		navsatfix.latitude = gps_reference_.x + m_to_lat_*msg->pose.pose.position.y;
		navsatfix.longitude = gps_reference_.y + m_to_lon_*msg->pose.pose.position.x;
		navsatfix.altitude = gps_reference_.z + msg->pose.pose.position.z;
	}

	pub_navsatfix_.publish(navsatfix);

	// Add to list for generation of kml file
	if(generate_kml_)
		coordinates_.push_back(navsatfix);

	// Generate MagneticField message
	sensor_msgs::MagneticField mag;
	mag.header = msg->header;

	Eigen::Quaterniond quat(msg->pose.pose.orientation.w,
			msg->pose.pose.orientation.x,
			msg->pose.pose.orientation.y,
			msg->pose.pose.orientation.z);

	Eigen::AngleAxisd yaw_correction(45*M_PI/180, Eigen::Vector3d::UnitZ());
	Eigen::Vector3d attitude = yaw_correction.matrix() *  // TODO: Solve this fix
				quat.toRotationMatrix() * Eigen::Vector3d::Ones();

	mag.magnetic_field.x = attitude[1];
	mag.magnetic_field.y = attitude[0];
	mag.magnetic_field.z = -attitude[2];

	pub_mag_.publish(mag);
}

NavsatfixConversion::~NavsatfixConversion()
{
	if(generate_kml_) {
		// Write in kml file
		outputFile_ << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" 									                << std::endl;
		outputFile_ << "<kml xmlns=\"http://www.opengis.net/kml/2.2\">" 								              << std::endl;
		outputFile_ << "  <Document>" 																	                              << std::endl;
		outputFile_ << "    <name>Paths</name>" 														                          << std::endl;
		outputFile_ << "    <description>autogenerated path from MSF framework</description>" 			  << std::endl;
		outputFile_ << "	<Style>" 																	                                  << std::endl;
		outputFile_ << "		<ListStyle>" 															                                << std::endl;
		outputFile_ << "			<listItemType>check</listItemType>" 								                    << std::endl;
		outputFile_ << "			<bgColor>00ffffff</bgColor>" 										                        << std::endl;
		outputFile_ << "			<maxSnippetLines>2</maxSnippetLines>" 								                  << std::endl;
		outputFile_ << "		</ListStyle>" 															                              << std::endl;
		outputFile_ << "	</Style>" 																	                                << std::endl;
		outputFile_ << "	<Style id=\"yellowLineGreenPoly\">" 										                    << std::endl;
		outputFile_ << "		<LineStyle>" 															                                << std::endl;
		outputFile_ << "			<color>d900ffff</color>" 											                          << std::endl;
		outputFile_ << "			<width>3</width>" 													                            << std::endl;
		outputFile_ << "		</LineStyle>" 															                              << std::endl;
		outputFile_ << "		<PolyStyle>" 															                                << std::endl;
		outputFile_ << "			<color>66000000</color>" 											                          << std::endl;
		outputFile_ << "			<outline>0</outline>" 												                          << std::endl;
		outputFile_ << "		</PolyStyle>" 															                              << std::endl;
		outputFile_ << "	</Style>" 																	                                << std::endl;
		outputFile_ << "	<Style id=\"yellowLineGreenPoly0\">" 										                    << std::endl;
		outputFile_ << "		<LineStyle>" 															                                << std::endl;
		outputFile_ << "			<color>d900ffff</color>" 											                          << std::endl;
		outputFile_ << "			<width>3</width>" 													                            << std::endl;
		outputFile_ << "		</LineStyle>" 															                              << std::endl;
		outputFile_ << "		<PolyStyle>" 															                                << std::endl;
		outputFile_ << "			<color>66000000</color>" 											                          << std::endl;
		outputFile_ << "			<outline>0</outline>" 											                            << std::endl;
		outputFile_ << "		</PolyStyle>" 															                              << std::endl;
		outputFile_ << "	</Style>" 																	                                << std::endl;
		outputFile_ << "	<StyleMap id=\"yellowLineGreenPoly1\">" 									                  << std::endl;
		outputFile_ << "		<Pair>" 																                                  << std::endl;
		outputFile_ << "			<key>normal</key>" 													                            << std::endl;
		outputFile_ << "			<styleUrl>#yellowLineGreenPoly0</styleUrl>" 						                << std::endl;
		outputFile_ << "		</Pair>" 																                                  << std::endl;
		outputFile_ << "		<Pair>" 																                                  << std::endl;
		outputFile_ << "			<key>highlight</key>" 												                          << std::endl;
		outputFile_ << "			<styleUrl>#yellowLineGreenPoly</styleUrl>" 						                  << std::endl;
		outputFile_ << "		</Pair>" 															                                    << std::endl;
		outputFile_ << "	</StyleMap>		" 														                                << std::endl;
		outputFile_ << "    <Placemark>" 																                              << std::endl;
		outputFile_ << "      <name>Absolute Extruded</name>" 											                  << std::endl;
		outputFile_ << "      <description>Transparent green wall with yellow outlines</description>" << std::endl;
		outputFile_ << "      <styleUrl>#yellowLineGreenPoly</styleUrl>"								              << std::endl;
		outputFile_ << "      <LineString>" 															                            << std::endl;
		outputFile_ << "        <extrude>1</extrude>" 													                      << std::endl;
		outputFile_ << "        <tessellate>1</tessellate>" 											                    << std::endl;
		outputFile_ << "        <altitudeMode>absolute</altitudeMode>" 									              << std::endl;
		outputFile_ << "        <coordinates>" 															                          << std::endl;

		// Iterate over list of coordinates
		for (std::list<sensor_msgs::NavSatFix>::iterator it = coordinates_.begin(); it != coordinates_.end(); it++)
			outputFile_ << std::setprecision(14) << "          " << (*it).longitude << ","
					    << (*it).latitude << "," << (*it).altitude << std::endl;

		outputFile_ << "        </coordinates>" 														                          << std::endl;
		outputFile_ << "      </LineString>" 															                            << std::endl;
		outputFile_ << "    </Placemark>" 																                            << std::endl;
		outputFile_ << "  </Document>" 																	                              << std::endl;
		outputFile_ << "</kml>" 																		                                  << std::endl;

		outputFile_.close();

		if(file_ok_)
			std::cout << "kml file successfully written!" << std::endl;
	}
}

/*
 * Copyright (C) 2015 Daniel Eckert, ASL, ETH Zurich, Switzerland
 * You can contact the author at <daeckert at student dot ethz dot ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "include/navsatfix_conversion.h"

int main(int argc, char** argv) {
  ros::init(argc, argv, "navsatfix_conversion");

  NavsatfixConversion navsatfix_conversion;

  ros::spin();

  return 0;
}

#!/usr/bin/env python

#
#  Title:        estimate_imu_bias.py
#  Description:  Tool to estimate gyroscope, accelerometer and magnetometer
#  bias for the Skybotix VI sensor 
#
#  The VI sensor has to be placed on even ground, with the front side facing
#  towards east
#

import rospy
#import sys
from visensor_msgs.msg import visensor_imu
from geometry_msgs.msg import Vector3
import time

# Settings
estimate_gyro = True
estimate_acc  = True
estimate_mag  = True
visensor_imu_topic = "/cust_imu0"

# Initialize bias variables
b_w = [0, 0, 0]
b_a = [0, 0, 0]
mag = [0, 0, 0]

k = 0
time = 0

def callback(msg):
    global k
    k += 1
    
    if(estimate_gyro):
        global b_w
        b_w[0] = (k-1)*b_w[0]/k + msg.angular_velocity.x/k
        b_w[1] = (k-1)*b_w[1]/k + msg.angular_velocity.y/k
        b_w[2] = (k-1)*b_w[2]/k + msg.angular_velocity.z/k
    if(estimate_acc):
        global b_a
        b_a[0] = (k-1)*b_a[0]/k + msg.linear_acceleration.x/k
        b_a[1] = (k-1)*b_a[1]/k + msg.linear_acceleration.y/k
        b_a[2] = (k-1)*b_a[2]/k + (msg.linear_acceleration.z + 9.81)/k
    if(estimate_mag):
        global mag
        mag[0] = (k-1)*mag[0]/k + msg.magnetometer.x/k
        mag[1] = (k-1)*mag[1]/k + msg.magnetometer.y/k
        mag[2] = (k-1)*mag[2]/k + msg.magnetometer.z/k
        
    # Publish estimates every second
    global time
    time_old = time
    time = rospy.get_rostime().secs
    if(time - time_old >= 1):
        if(estimate_gyro):
            rospy.logwarn("\nb_w: \n'%s'", b_w)
        if(estimate_acc):
            rospy.logwarn("\nb_a: \n'%s'", b_a)
        if(estimate_mag):
            rospy.logwarn("\nmag: \n'%s'", mag)
        
# Main function.
if __name__ == '__main__':   
    rospy.init_node('estimate_imu_bias')
    
    rospy.Subscriber(visensor_imu_topic, visensor_imu, callback)
    rospy.spin()         